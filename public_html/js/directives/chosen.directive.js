(function() {

    var chosenDirective = function(timeout) {
        return {
            restrict: 'A',
            scope: {ngModel: "=", ngDisabled: "=", chosenDirective: "="},
            link: function(scope, element, attrs) {
                timeout(function() {
                    $(element).chosen({disable_search_threshold: 10});
                    scope.$watch("ngModel", function(value) {
                        if (value) {
                            timeout(function() {
                                $(element).trigger("chosen:updated");
                            });
                        }
                        else {
                            element.trigger("chosen:updated");
                        }
                    }, true);
//                scope.$watch("chosenDirective", function(value) {
//                    if (value) {
//                        
//                        timeout(function() {
//                            $(element).trigger("chosen:updated");
//                        });
//                    }
//                }, true);
                    scope.$watch("ngDisabled", function(value) {
                        element.trigger("chosen:updated");
                    });
                    scope.$watch("chosenDirective", function(value) {
                        if (value) {
                            timeout(function() {
                                element.trigger("chosen:updated");
                            });
                        }
                    }, true);
                    timeout(function() {
                        element.trigger("chosen:updated");
                    });

                });

                $(document).on('click', '#fix-col table tbody tr:nth-last-child(3) .chosen-single', function(event) {
                    event.stopPropagation();
                    $('#fix-col table tbody tr:last-child').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col table tbody tr:nth-last-child(2)').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col').css('padding-bottom', '20px');
                });
                $(document).on('click', '#fix-col table tbody tr:nth-last-child(2) .chosen-single', function(event) {
                    event.stopPropagation();
                    $('#fix-col table tbody tr:last-child').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col table tbody tr:nth-last-child(3)').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col').css('padding-bottom', '60px');
                });

                $(document).on('click', '#fix-col table tbody tr:last-child .chosen-single', function(event) {
                    event.stopPropagation();
                    $('#fix-col table tbody tr:nth-last-child(2)').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col table tbody tr:nth-last-child(3)').find('.chosen-with-drop').removeClass('chosen-with-drop');
                    $('#fix-col').css('padding-bottom', '100px');
                });

                $('body').on('click', function() {
                    $('#fix-col').css('padding-bottom', '0');
                });
            }
        };
    };
    angular.module('twala.directives').directive('chosenDirective', ['$timeout', chosenDirective]);
})();