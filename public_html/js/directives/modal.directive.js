(function() {
    var modalDir = function() {
        return {
            restrict: "A",
            scope: {
                modalShow: "="
            },
            link: function(scope, element, attrs) {
                //Hide or show the modal
                scope.showModal = function(visible) {
                    if (visible) {
                        element.modal("show");
                    } else {
                        element.modal("hide");
                        $('.modal-backdrop').remove();
                    }
                }
                //Check to see if the modal-visible attribute exists
                if (!attrs.modalShow) {
                    //The attribute isn't defined, show the modal by default
                    scope.showModal(true);
                } else {
                    //Watch for changes to the modal-visible attribute
                    scope.$watch("modalShow", function(newValue) {
                        scope.showModal(newValue);
                    });
                    //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
                    element.bind("hide.bs.modal", function() {
                        scope.modalShow = false;
                        if (!scope.$$phase && !scope.$root.$$phase)
                            scope.$apply();
                    });
                }
            }
        };
    };
    angular.module('twala.directives').directive('modalShow', [modalDir]);
})();
