(function() {
    function RegisterSalesPersonController($scope, $rootScope, UserService, Upload, http) {
        $scope.user = angular.copy($rootScope.currentUser);
        $scope.updateProfile = function() {
            $scope.submitted = true;
            if ($scope.profileForm.$valid) {
                UserService.updateUser($scope.user, function(result) {
                    $rootScope.currentUser = angular.copy(result.data);
                }, function() {
                });
            }
        };

        $scope.upload = function(file) {
            Upload.upload({
                url: $rootScope.apiPath + '/users/uploadprofilepicture',
                file: file
            }).then(function(resp) {
                http.get($rootScope.apiPath + '/common/session').success(function(data) {
                    $rootScope.currentUser = data.currentUser;
                });
            }, function(resp) {
                http.get($rootScope.apiPath + '/common/session').success(function(data) {
                    $rootScope.currentUser = data.currentUser;
                });
            }, function(evt) {
            });
        };
    }
    angular.module('twala.controllers').controller('ProfileController', ['$scope', "$rootScope", "UserService", "Upload", "$http", RegisterSalesPersonController]);
})();
