(function () {
    function LocationController($scope, $rootScope, SearchService, $routeParams, ServiceProviderService, $location) {
        $scope.searchObj = {mealSelected: [], prefSelected: [], min: 0, max: 500};
        $scope.min = 0;
        $scope.max = 500;
        $scope.searchObj.q = $routeParams.q;
        $scope.sortType = 'relevance';
        $scope.sortReverse = true;
        $scope.currentPage = 1;
        $scope.searchQuery = function () {
            var payload = {
                q: $scope.searchObj.q,
                meal: $scope.searchObj.mealSelected !== undefined && $scope.searchObj.mealSelected !== null ? $scope.searchObj.mealSelected.toString() : null,
                pref: $scope.searchObj.prefSelected !== undefined && $scope.searchObj.prefSelected !== null ? $scope.searchObj.prefSelected.toString() : null,
                hours: $scope.searchObj.hours,
                minprice: $scope.searchObj.min,
                maxprice: $scope.searchObj.max,
                homeDeliveryAvail: $scope.searchObj.homeDeliveryAvail,
                takesPartyOrder: $scope.searchObj.takesPartyOrder,
                customizeTiffin: $scope.searchObj.customizeTiffin,
                advancePayment: $scope.searchObj.advancePayment,
                sort: $scope.sortType,
                order: $scope.sortReverse === true ? 'desc' : 'asc',
                start: ($scope.currentPage - 1) * 10
            };
            $scope.query = $scope.searchObj;
            $location.path('/location').search(payload);
            SearchService.search(payload, function (result) {
                if (!!result) {
                    $scope.totalResults = result.totalResults;
                    $scope.getNumber($scope.totalResults);
                    $scope.resultEntitys = result["resultEntitys"];
                    if (!$scope.resultEntitys.length)
                    {
                        $scope.emailId = $rootScope.currentUser.email;
                    }
                }
            });
        };
        $scope.getNumber = function (num) {
            console.log("num:::" + num)
            if (num !== undefined && num !== null)
                $scope.arr = new Array(Math.ceil(num / 10));
        };
        $scope.setPage = function (n) {
            $scope.currentPage = n;
            $scope.searchQuery();
        };
        $scope.prevPage = function () {
            $scope.currentPage = $scope.currentPage - 1;
            $scope.searchQuery();
        };
        $scope.nextPage = function () {
            $scope.currentPage = $scope.currentPage + 1;
            $scope.searchQuery();
        };
        $scope.toggleSelection = function (mealName) {
            var idx = $scope.searchObj.mealSelected.indexOf(mealName);
            if (idx > -1) {
                $scope.searchObj.mealSelected.splice(idx, 1);
            } else {
                $scope.searchObj.mealSelected.push(mealName);
            }
            $scope.searchQuery();
        };
        $scope.togglePrefSelection = function (prefName) {
            var idx = $scope.searchObj.prefSelected.indexOf(prefName);
            if (idx > -1) {
                $scope.searchObj.prefSelected.splice(idx, 1);
            } else {
                $scope.searchObj.prefSelected.push(prefName);
            }
            $scope.searchQuery();
        };
        $scope.toggleHomeDelivery = function (value) {
            $scope.searchObj.homeDeliveryAvail = value;
            $scope.searchQuery();
        };
        $scope.toggleTakesPartyOrder = function (value) {
            $scope.searchObj.takesPartyOrder = value;
            $scope.searchQuery();
        };
        $scope.toggleCustomizeTiffin = function (value) {
            $scope.searchObj.customizeTiffin = value;
            $scope.searchQuery();
        };
        $scope.toggleAdvancePayment = function (value) {
            $scope.searchObj.advancePayment = value;
            $scope.searchQuery();
        };
        $scope.setModelVal = function () {
            $scope.searchObj.min = $scope.min;
            $scope.searchObj.max = $scope.max;
            $scope.searchQuery();
        };
        $scope.storeQuery = function (emailId) {
            $scope.query["email"] = emailId;
            SearchService.search($scope.query, function (result) {
                $scope.successTextAlert = "Your query submitted successfully.";
                $scope.showSuccessAlert = true;
            });
        };
        $scope.switchBool = function (value) {
            $scope[value] = !$scope[value];
        };
        $scope.unreachable = function (entity) {
            if (!!entity && $rootScope.isLogin) {
                ServiceProviderService.unreachable(entity.entity.id, function (res) {
                    $scope.showSuccessAlert = true;
                    $scope.successTextAlert = "Tiffin provider marked as unreachable";
                });
            } else if (!$rootScope.isLogin) {
                //Show login modal
            }
        };
        $scope.mailContact = function (entity) {
            if (!!entity && $rootScope.isLogin) {
                console.log("$rootScope.currentUser:::" + JSON.stringify($rootScope.currentUser));
                if ($rootScope.currentUser.email !== null && $rootScope.currentUser.email !== undefined) {
                    ServiceProviderService.mailContact({id: entity.entity.id, email: $rootScope.currentUser.email}, function (res) {
                        $scope.showSuccessAlert = true;
                        $scope.successTextAlert = "Contact detail mailed successfully. Please check your mail";
                    });
                } else {
                    console.log("Mail id not available");
                }

            } else if (!$rootScope.isLogin) {
//Show login modal
            }
        };

        $scope.sortBySP = function (param) {
            if (param === $scope.sortType)
                $scope.sortReverse = !$scope.sortReverse;
            else
                $scope.sortReverse = true;

            $scope.sortType = param;

            $scope.searchQuery();
        };
        $scope.appendTimeFilter = function () {
            var fromTime = ConvertTimeformat($scope.fromTime, $scope.fromTimeZone);
            var toTime = ConvertTimeformat($scope.toTime, $scope.toTimeZone);
            var arrOfHours = [];
            if (fromTime < toTime) {
                for (var index = fromTime; index <= toTime; index++) {
                    arrOfHours.push(index);
                }
            } else if (fromTime > toTime) {
                for (var index = fromTime; index <= 24; index++) {
                    arrOfHours.push(index);
                }
                for (var index = 1; index <= toTime; index++) {
                    arrOfHours.push(index);
                }
            } else {
                arrOfHours.push(fromTime);
            }
            $scope.searchObj.hours = arrOfHours.join(" ");
            $scope.searchQuery();
        };
        function ConvertTimeformat(time, AMPM) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            if (AMPM === "PM" && hours < 12)
                hours = hours + 12;
            if (AMPM === "AM" && hours === 12)
                hours = hours - 12;
            var sHours = hours.toString();
            if (hours < 10)
                sHours = "0" + sHours;
            return Number(sHours);
        }
        $scope.searchQuery();
    }
    angular.module('twala.controllers').controller('LocationController', ['$scope', '$rootScope', "SearchService", "$routeParams", "ServiceProviderService", "$location", LocationController]);
})();
