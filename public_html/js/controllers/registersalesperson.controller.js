(function() {
    function RegisterSalesPersonController($scope, $rootScope, UserService, Upload) {

        UserService.retrieveCities(function(result) {
            $scope.cities = angular.copy(result.data);
        });
        $scope.createAccount = function() {
            $scope.submitted = true;
            if ($scope.signupForm.$valid) {
                $scope.user.role = "ROLE_SALES";
                UserService.registerSP($scope.user, function(result) {
                }, function() {
                });
            }
        };

        $scope.upload = function(file) {
            Upload.upload({
                url: $rootScope.apiPath + '/users/uploadprofilepicture',
                file: file
            }).progress(function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            }).success(function(data, status, headers, config) {
            });
        };

        $scope.checkUsernameAvailability = function() {
            if ($scope.user.userName !== undefined && $scope.user.userName !== null) {
                UserService.checkUserExist({"username": $scope.user.userName}, function(res) {
                    $scope.signupForm.uname.$setValidity("unique", !res.data);
                }, function() {
                });
            }
        };
    }
    angular.module('twala.controllers').controller('RegisterSalesPersonController', ['$scope', "$rootScope", "UserService", "Upload", RegisterSalesPersonController]);
})();
