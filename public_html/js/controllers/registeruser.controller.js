(function() {
    function RegisterUserController($scope, $rootScope, UserService, Upload, http, InsecureService, ServiceProviderService) {
        $('#myModal').modal('hide');
        $scope.user = {utype: 'user', profilePicturePath: null};

        $scope.saveUser = function() {
            $scope.submitted = true;
            if ($scope.userRegForm.$valid) {
                var payload = {
                    firstName: $scope.user.firstName,
                    lastName: $scope.user.lastName,
                    userName: $scope.user.userName,
                    password: $scope.user.password,
                    email: $scope.user.email,
                    role: "ROLE_USER",
                    phone: $scope.user.phone,
                    profilePicture: $scope.user.profilePicturePath
                };

                InsecureService.createUser(payload, function(res) {
                    if (!!res && !!res.messages && res.messages.length > 0) {
                        $scope.showDangerAlert = true;
                        $scope.dangerTextAlert = res.messages[0].message;
                    } else {
                        $scope.showSuccessAlert = true;
                        $scope.successTextAlert = "User registered successfully.";
                        $scope.user = {utype: 'user'};
                        $scope.userRegForm.$setUntouched();
                        $scope.submitted = false;
                    }
                });
            }
        };

        $scope.saveSP = function() {
            $scope.submitted = true;
            if ($scope.userRegForm.$valid) {
                var payload = {
                    name: $scope.user.spName,
                    mobileNumber: $scope.user.mobileNumber,
                    address: $scope.user.address
                };
                ServiceProviderService.spReq(payload, function(res) {
                    if (!!res && !!res.messages && res.messages.length > 0) {
                        $scope.showDangerAlert = true;
                        $scope.dangerTextAlert = res.messages[0].message;
                    } else {
                        $scope.showSuccessAlert = true;
                        $scope.successTextAlert = "Service Provider registered successfully.";
                        $scope.user = {utype: 'sp'};
                        $scope.userRegForm.$setUntouched();
                        $scope.submitted = false;
                    }
                });
            }
        };
        $scope.switchBool = function(value) {
            $scope[value] = !$scope[value];
        };
        $scope.upload = function(file) {
            Upload.upload({
                url: $rootScope.apiPath + '/insecured/uploadprofilepicture',
                file: file
            }).then(function(resp) {
//                console.log(resp);
                $scope.user.profilePicturePath = resp.data.path;
            }, function(resp) {
//                console.log("Success result::" + resp);
            }, function(evt) {
            });
        };
    }
    angular.module('twala.controllers').controller('RegisterUserController', ['$scope', "$rootScope", "UserService", "Upload", "$http", "InsecureService", "ServiceProviderService", RegisterUserController]);
})();
