(function() {
    function ContactUsController($scope, InsecureService) {
        $scope.contactus = {};
        $scope.contactUs = function() {
            $scope.submitted = true;
            if ($scope.contactusForm.$valid) {
                InsecureService.contactus($scope.contactus, function(result) {
                    $scope.showSuccessAlert = true;
                    $scope.successTextAlert = "Your details submitted successfully.";
                    $scope.contactus = {};
                    $scope.contactusForm.$setUntouched();
                    $scope.submitted = false;
                }, function() {
                });
            }
        };
        $scope.switchBool = function(value) {
            $scope[value] = !$scope[value];
        };
    }
    angular.module('twala.controllers').controller('ContactUsController', ['$scope', "InsecureService", ContactUsController]);
})();
