(function() {
    function MenuController($scope, MenuService) {

        $scope.selectedItems = [];
        $scope.itemOptionIdToIndex = {};
        $scope.searchItemOption = function() {
            if (!!$scope.menu.name) {
                MenuService.retrieveItemOptionByName({"name": $scope.menu.name}, function(res) {
                    $scope.itemOptionList = res.data;
                });
            }
        };

        $scope.addOrRemoveItemOption = function(item) {
            if (!!item) {
                if ($scope.selectedItems === undefined || $scope.selectedItems === null || $scope.selectedItems.indexOf(item.id) === -1) {
                    var payload = {"itemOption": item.id, "menuIdentifier": $scope.menuIdentifier};
                    MenuService.addMenuItem(payload, function(index) {
                        $scope.selectedItems.push(item.id);
                        $scope.itemOptionIdToIndex[item.id] = index.data;
                    });
                } else {
                    //Remove
                    var payload = {"index": $scope.itemOptionIdToIndex[item.id], "menuIdentifier": $scope.menuIdentifier};
                    MenuService.deleteMenuItem(payload, function() {
                        $scope.selectedItems.splice($scope.selectedItems.indexOf(item.id), 1);
                        delete $scope.itemOptionIdToIndex[item.id];
                    });
                }
            }
        };
        $scope.verify = function() {
            if (!!$scope.menuIdentifier) {
                MenuService.verifyMenuItem({"menuId": $scope.menuIdentifier}, function(res) {
                    $scope.verifyData = res.data;
                });
            }
        };
        $scope.confirm = function() {
            if (!!$scope.menuIdentifier) {
                MenuService.confirmMenuItem($scope.menuIdentifier, function(res) {
                    $scope.successTextAlert = "Menu updated successfully.";
                    $scope.showSuccessAlert = true;
                });
            }
        };
        $scope.switchBool = function(value) {
            $scope[value] = !$scope[value];
        };
        $scope.menu = {};
        $scope.startMenu = function() {
            $scope.submitted = true;
            if ($scope.menuForm.$valid) {
                $scope.menu.menuDate = new Date();
                MenuService.startMenu($scope.menu, function(res) {
                    $scope.menu = {};
                    $scope.selectedItems = [];
                    $scope.itemOptionIdToIndex = {};
                    $scope.submitted = false;
                    $scope.menuStarted = true;
                    $scope.menuIdentifier = res.data;
                    MenuService.verifyMenuItem({"menuId": $scope.menuIdentifier}, function(res) {
                        $scope.verifyData = res.data;
                        if (res.data.menuItemDtos !== undefined && res.data.menuItemDtos !== null && res.data.menuItemDtos.length > 0) {
                            angular.forEach(res.data.menuItemDtos, function(item) {
                                $scope.selectedItems.push(item.itemOption);
                                $scope.itemOptionIdToIndex[item.itemOption] = item.index;
                            });
                        }
                    });
                });
            }
        };
    }
    angular.module('twala.controllers').controller('MenuController', ['$scope', "MenuService", MenuController]);
})();
