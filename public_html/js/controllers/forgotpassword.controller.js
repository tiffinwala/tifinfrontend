(function() {
    function ForgotPasswordController($scope, $routeParams, InsecureService) {

        $scope.email = $routeParams.email;
        $scope.token = $routeParams.token;
        $scope.isValidateEmailToken = null;
        InsecureService.validateEmailToken({email: $scope.email, token: $scope.token}, function(res) {
            $scope.isValidateEmailToken = res.data;
            if ($scope.isValidateEmailToken === false) {
                $scope.responseCode = 1;
                $scope.successTextAlert = res.messages[0].message;
                $scope.showSuccessAlert = true;
            }
        });
        $scope.switchBool = function(value) {
            $scope[value] = !$scope[value];
        };
        $scope.setNewPassword = function(form, pass, repass) {
            if (!!form && form.$valid) {
                var payload = {
                    email: $scope.email,
                    token: $scope.token,
                    password: pass,
                    repassword: repass
                };
                InsecureService.savePassword(payload, function(res) {
                    $scope.responseCode = res.messages[0].responseCode;
                    $scope.successTextAlert = res.messages[0].message;
                    $scope.showSuccessAlert = true;
                });
            }
        };
    }
    angular.module('twala.controllers').controller('ForgotPasswordController', ['$scope', "$routeParams", "InsecureService", ForgotPasswordController]);
})();
