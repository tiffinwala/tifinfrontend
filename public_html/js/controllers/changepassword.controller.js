(function() {
    function ChangePasswordController($scope, UserService) {
        $scope.switchBool = function(value) {
            $scope[value] = !$scope[value];
        };
        $scope.setNewPassword = function(form) {
            $scope.submitted = true;
            if (!!form && form.$valid) {
                var payload = {
                    oldPassword: $scope.oldPassword,
                    newPassword: $scope.newPassword,
                    repeatNewPassword: $scope.repassword
                };
                UserService.changePassword(payload, function(res) {
                    $scope.responseCode = res.messages[0].responseCode;
                    $scope.successTextAlert = res.messages[0].message;
                    $scope.showSuccessAlert = true;
                });
            }
        };
    }
    angular.module('twala.controllers').controller('ChangePasswordController', ['$scope', "UserService", ChangePasswordController]);
})();
