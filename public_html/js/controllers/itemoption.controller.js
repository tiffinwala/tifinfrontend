(function() {
    function ItemOptionController($scope, $rootScope, MenuService) {

        MenuService.retrieveItemTypes(function(result) {
            $scope.itemTypes = result.data;
        });

        $scope.createItemOption = function() {
            $scope.submitted = true;
            if ($scope.itemoptionForm.$valid) {
                MenuService.createItemOptions($scope.itemoption, function(result) {
                    $scope.itemoption = {};
                    $scope.itemoptionForm.$setUntouched();
                    $scope.submitted = false;
                }, function() {
                });
            }
        };

    }
    angular.module('twala.controllers').controller('ItemOptionController', ['$scope', "$rootScope", "MenuService", ItemOptionController]);
})();
