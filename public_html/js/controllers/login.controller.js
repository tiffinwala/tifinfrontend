(function() {
    function LoginController($rootScope, $http) {
        var ctrl = this;
        $rootScope.companyLogo = $rootScope.defaultLogo;
        ctrl.isError = false;
        var ctrl = this;
        ctrl.errorMessage = '';
        ctrl.userObject = {};
        var data = localStorage.getItem('usera');
        data = JSON.parse(data);
        $rootScope.pingServer();
        if (data !== null) {
            if (!!data.rememberme) {
                ctrl.userObject.rememberme = data.rememberme;
                ctrl.userObject.emailId = data.emailId;
                ctrl.userObject.password = $rootScope.encryptPass(data.password, false)
            }
        }
        ctrl.doLogin = function(userObject, loginform) {
            ctrl.loginSubmitted = true;
            ctrl.isError = false;
            if (loginform.$valid) {
                console.log("run : login request event");
                var payload = $.param({
                    xfjgtn: userObject.emailId,
                    tyijrd: userObject.password,
                    zxcvbnm: userObject.rememberme
                });
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                };
                $http.post($rootScope.apiPath + '/users/login', payload, config).success(function(data, status) {
                    console.log("run : login request event : post sucess : ");
                    if (!!userObject.rememberme) {
                        var user = {
                            emailId: userObject.emailId,
                            password: $rootScope.encryptPass(userObject.password, true),
                            rememberme: userObject.rememberme
                        };
                        localStorage.setItem('usera', JSON.stringify(user));
                    } else {
                        localStorage.removeItem('usera');
                    }
                    $rootScope.pingServer();
                }).error(function(data, status) {
                    if (status === 401) {
                        ctrl.errorMessage = data;
                        ctrl.isError = true;
                    }
                });
            }
        };
    }
    angular.module('twala.controllers').controller('LoginController', ['$rootScope', '$http', LoginController]);
})();
