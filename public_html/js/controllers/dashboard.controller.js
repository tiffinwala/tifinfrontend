(function () {
    function DashboardController($scope, $rootScope, SearchService ) {
        var dashboard = this;

        $scope.$watch('dashboard.chosenPlaceDetails', function (newVal, oldVal) {
            console.log(newVal['formatted_address']);
            var q = newVal['formatted_address']
//            var getUrl = $rootScope.apiPath + '/search?q=' + q;

           SearchService.search({q : q});
        });

    }
    angular.module('twala.controllers').controller('DashboardController', ["$scope", "$rootScope", "SearchService", DashboardController]);
})();
