(function () {
    function MainController($scope, $rootScope, $location, $timeout, $http, SearchService, InsecureService) {
        var ctrl = this;
        //Date format for jquery datapicker
        $rootScope.dateFormat = 'dd/mm/yy';
        //Date format for angular 
        $rootScope.angularDateFormat = 'dd/MM/yy';
        $rootScope.printableChars = '~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:"ZXCVBNM<>?`1234567890-=qwertyuiop[]\\asdfghjkl;\'zxcvbnm,./';
        $rootScope.showForgetPassword = false;
        $scope.fbAuthenticated = false;
        $scope.$watch("alerts", function (alerts) {
            if (alerts != null && alerts.length > 0) {
                $timeout(function () {
                    alerts[0].expired = true;
                    $timeout(function () {
                        $rootScope.alerts = [];
                    }, 3000);
                }, 3000);
            }
        }, true);
        $scope.isCurrent = function (path) {
            if ((path === '/home') && $location.path() === "/") {
                return true;
            } else {
                if ($location.path().substr(0, path.length) === path) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        //To scroll to top of the page on new page
        $scope.$on('$locationChangeSuccess', function () {
            $rootScope.unAuthorizedAlerts = [];
            $scope.scrollFlag = true;
        });

        $rootScope.encryptPass = function (password, isEnctrypt) {
            if (password !== null && password !== undefined && password !== "")
            {
                var passLength = password.length;
                var newPass = "";
                for (var i = 0; i < passLength; i++)
                {
                    var temp = password.charAt(i);
                    var index = $rootScope.printableChars.indexOf(temp);
                    if (index !== -1)
                    {
                        if (isEnctrypt)
                        {
                            index += (i + passLength);
                            if (index >= ($rootScope.printableChars.length))
                            {
                                index -= ($rootScope.printableChars.length);
                            }
                        } else {
                            index -= (i + passLength);
                            if (index < 0)
                            {
                                index += ($rootScope.printableChars.length);
                            }
                        }
                        temp = $rootScope.printableChars.charAt(index);
                        newPass += temp;
                    } else {
                        newPass += temp;
                    }
                }
                return newPass;
            } else {
                return password;
            }
        };

        $rootScope.openEmailPage = function (name) {
            $location.path("email/" + name);
            $location.path();
        };

        $rootScope.openImagePage = function (name) {
            if (name == 'manage') {
                $location.path("images");
            } else {
                $location.path("imageload");
            }
            $location.path();
        };

        $rootScope.openDashboardPage = function (name) {
            $location.path("home/" + name);
            $location.path();
        };

        $rootScope.logout = function () {
            $rootScope.currentUser = null;
//            $scope.username = scope.password = null;
            $rootScope.isLogin = false;
            $rootScope.$emit('event:logoutRequest');
        };
        $scope.isError = false;
        $scope.errorMessage = '';
        $scope.userObject = {};
        var data = localStorage.getItem('usera');
        data = JSON.parse(data);
        $rootScope.pingServer();
        if (data !== null) {
            if (!!data.rememberme) {
                ctrl.userObject.rememberme = data.rememberme;
                ctrl.userObject.emailId = data.emailId;
                ctrl.userObject.password = $rootScope.encryptPass(data.password, false)
            }
        }
        $scope.doLogin = function (userObject, loginform) {
            $scope.loginSubmitted = true;
            $scope.isError = false;
            if (loginform.$valid || !!$scope.fbAuthenticated) {
                console.log("fbAuthenticated : " + !!$scope.fbAuthenticated);
                console.log("run : login request event");
                var payload = $.param({
                    xfjgtn: userObject.emailId,
                    tyijrd: userObject.password,
                    zxcvbnm: userObject.rememberme
                });

                if (!!$scope.fbAuthenticated) {
                    payload = $.param({
                        xfjgtn: userObject.fbId,
                        tyijrd: userObject.fbId,
                        email: userObject.emailId,
                        fname: userObject.fname,
                        lname: userObject.lname,
                        name: userObject.name,
                        profilePic: userObject.profilePic,
                        gender: userObject.gender,
                        fbId: userObject.fbId
                    });
                }

                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                };
                $http.post($rootScope.apiPath + '/users/login', payload, config).success(function (data, status) {
                    console.log("run : login request event : post sucess : ");
                    if (!!userObject.rememberme) {
                        var user = {
                            emailId: userObject.emailId,
                            password: $rootScope.encryptPass(userObject.password, true),
                            rememberme: userObject.rememberme
                        };
                        localStorage.setItem('usera', JSON.stringify(user));
                    } else {
                        localStorage.removeItem('usera');
                    }
                    $rootScope.pingServer();
                    $('#myModal').modal('toggle');
                }).error(function (data, status) {
                    if (status === 401) {
                        $scope.errorMessage = data;
                        $scope.isError = true;
                    }
                });
            }
        };
        $rootScope.$watch('chosenPlace', function (newVal, oldVal) {
            console.log("In........" + JSON.stringify(newVal));
            var q = newVal['formatted_address'];
            $rootScope.place = q;
//            SearchService.search({q: q});
        });

        $rootScope.search = function (form) {
            console.log("$scope.searchquery:::" + form);
            if (form.$valid) {
                $location.path('/location/').search({q: $rootScope.place});
            }
        };
        $rootScope.forgetPassword = function (email) {
            InsecureService.forgetPassword(email, function (res) {
                console.log("res::::" + JSON.stringify(res));
            });
        };
        function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            if (response.status === 'connected') {
                testAPI();
            } else if (response.status === 'not_authorized') {
            } else {
            }
        }
        function checkLoginState() {
            FB.login(function (response) {
                console.log("1....calling statusChangeCallback.....");
                statusChangeCallback(response);
                // handle the response
            }, {scope: 'email,user_likes'});
        }
        $scope.checkLoginState = function () {
            FB.login(function (response) {
                console.log("1....calling statusChangeCallback.....");
                statusChangeCallback(response);
                // handle the response
            }, {scope: 'email,user_likes'});
        }

        window.fbAsyncInit = function () {
            FB.init({
                appId: '968471476558720',
                cookie: true, // enable cookies to allow the server to access 
                // the session
                xfbml: true, // parse social plugins on this page
                version: 'v2.5' // use version 2.5
            });
        };

        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        function testAPI() {
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', {fields: 'email,first_name,last_name,name,location,gender,picture'}, function (response) {
                console.log(JSON.stringify(response));
                console.log('Successful login for: ' + response.name);
                var userObject = {};
                userObject.emailId = response.email;
                userObject.fname = response.first_name;
                userObject.lname = response.last_name;
                userObject.profilePic = response.picture.data.url;
                userObject.gender = response.gender;
                userObject.fbId = response.id;
                $scope.loginForm.$setPristine();
                $scope.loginForm.$setUntouched();
                $scope.fbAuthenticated = true;
                $scope.doLogin(userObject, $scope.loginForm);
            });
        }

        $rootScope.openMyProfile = function () {
            $location.url("profile");
        };

        $rootScope.openChangePwd = function () {
            $location.url("changepassword");
        };

        $rootScope.openUpdateMenu = function () {
            $location.url("menu");
        };

        $rootScope.pingServer();
    }
    angular.module('twala.controllers').controller('MainController', ['$scope', '$rootScope', "$location", "$timeout", "$http", "SearchService", "InsecureService", MainController]);
})();
