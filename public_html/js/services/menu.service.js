(function() {
    angular.module('twala.service').factory('MenuService', ['$resource', '$rootScope', function(resource, rootScope) {
            var menuService = resource(rootScope.apiPath + '/menu/:action/:id', {}, {
                retrieveItemTypes: {
                    method: 'GET',
                    params: {
                        action: "retrieveitemtypes"
                    }
                },
                updateUser: {
                    method: 'PUT'
                },
                createItemOptions: {
                    method: 'POST',
                    params: {
                        action: "itemoption/create"
                    }
                },
                retrieveItemOptions: {
                    method: 'GET',
                    params: {
                        action: "itemoptions"
                    }
                },
                retrieveItemOptionById: {
                    method: 'GET',
                    params: {
                        action: "itemoptions/retrieve"
                    }
                },
                updateItemOption: {
                    method: 'POST',
                    params: {
                        action: "itemoption/update"
                    }
                },
                inactiveItemOption: {
                    method: 'GET',
                    params: {
                        action: "itemoption/inactive"
                    }
                },
                retrieveItemOptionByName: {
                    method: 'GET',
                    params: {
                        action: "itemoption/retrievebyname"
                    }
                },
                startMenu: {
                    method: 'POST',
                    params: {
                        action: "startmenu"
                    }
                },
                addMenuItem: {
                    method: 'POST',
                    params: {
                        action: "new/add"
                    }
                },
                verifyMenuItem: {
                    method: 'GET',
                    params: {
                        action: "new/verify"
                    }
                },
                confirmMenuItem: {
                    method: 'POST',
                    params: {
                        action: "new/confirm"
                    }
                },
                deleteMenuItem: {
                    method: 'POST',
                    params: {
                        action: "new/delete"
                    }
                },
                editMenuItem: {
                    method: 'POST',
                    params: {
                        action: "new/edit"
                    }
                }
            });
            return menuService;
        }]);
})();