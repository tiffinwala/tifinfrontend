(function() {
    angular.module('twala.service').factory('ServiceProviderService', ['$resource', '$rootScope', function(resource, rootScope) {
            var menuService = resource(rootScope.apiPath + '/sps/:action/', {}, {
                unreachable: {
                    method: 'POST',
                    params: {
                        action: "unreach"
                    }
                }, 
                mailContact: {
                    method: 'GET',
                    params: {
                        action: "mailcontact"
                    }
                }, 
                spReq: {
                    method: 'POST',
                    params: {
                        action: "spreq"
                    }
                }
            });
            return menuService;
        }]);
})();