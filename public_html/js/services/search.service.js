(function() {
    angular.module('twala.service').factory('SearchService', ['$resource', '$rootScope', function(resource, rootScope) {
            var organizationService = resource(rootScope.apiPath + '/search/:action', {}, {
                search: {
                    method: 'GET',
//                    isArray : true
//                    params: {
//                        action: 'updatestatus'
//                    }
                }
            });
            return organizationService;
        }]);
})();