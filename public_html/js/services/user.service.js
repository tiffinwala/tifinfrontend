(function() {
    angular.module('twala.service').factory('UserService', ['$resource', '$rootScope', function(resource, rootScope) {
            var userService = resource(rootScope.apiPath + '/users/:action/:id', {}, {
                registerSP: {
                    method: 'POST'
                },
                retrieveUserById: {
                    method: 'GET',
                    params: {
                        action: "retrieve"
                    }
                },
                updateUser: {
                    method: 'PUT'
                },
                checkUserExist: {
                    method: 'GET',
                    params: {
                        action: "existusername"
                    }
                },
                retrieveCities: {
                    method: 'GET',
                    params: {
                        action: "retrievecities"
                    }
                },
                changePassword: {
                    method: 'GET',
                    params: {
                        action: "savepassword"
                    }
                }
            });
            return userService;
        }]);
})();