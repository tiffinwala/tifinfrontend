(function() {
    angular.module('twala.service').factory('InsecureService', ['$resource', '$rootScope', function(resource, rootScope) {
            var menuService = resource(rootScope.apiPath + '/insecured/:action/:id', {}, {
                forgetPassword: {
                    method: 'POST',
                    params: {
                        action: "forgetpassword"
                    }
                },
                validateEmailToken: {
                    method: 'GET',
                    params: {
                        action: "validateemailtoken"
                    }
                },
                savePassword: {
                    method: 'POST',
                    params: {
                        action: "savepassword"
                    }
                },
                contactus: {
                    method: 'POST',
                    params: {
                        action: "contact"
                    }
                },
                createUser: {
                    method: 'POST',
                    params: {
                        action: "createuser"
                    }
                }
            });
            return menuService;
        }]);
})();