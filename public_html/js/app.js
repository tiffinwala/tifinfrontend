(function() {
    var as = angular.module("twala", ["ngRoute", "twala.controllers", "twala.service", "twala.directives", "ngMessages", "ngFileUpload", "ui-rangeSlider", "google.places"]);
    as.config(function($routeProvider, $provide, $httpProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/home.html',
            controller: 'DashboardController as dashboard'
        }).when('/registersalesperson', {
            templateUrl: 'views/registerSalesPerson.html',
            controller: 'RegisterSalesPersonController'
        }).when('/profile', {
            templateUrl: 'views/myprofile.html',
            controller: 'ProfileController'
        }).when('/itemoption', {
            templateUrl: 'views/itemoption.html',
            controller: 'ItemOptionController'
        }).when('/menu', {
            templateUrl: 'views/menu.html',
            controller: 'MenuController'
        }).when('/location', {
            templateUrl: 'views/location.html',
            controller: 'LocationController',
            reloadOnSearch: false
        }).when('/forgetpassword', {
            templateUrl: 'views/forgotpassword.html',
            controller: 'ForgotPasswordController'
        }).when('/changepassword', {
            templateUrl: 'views/changepassword.html',
            controller: 'ChangePasswordController'
        }).when('/contactus', {
            templateUrl: 'views/contactus.html',
            controller: 'ContactUsController'
        }).when('/registeruser', {
            templateUrl: 'views/registerUser.html',
            controller: 'RegisterUserController'
        });
        // added for handling httpRequest
        var httpRequests = new Array();

        // Add the interceptor to the $httpProvider.
//        $httpProvider.interceptors.push('HttpRequestResponseInterceptor');
        $httpProvider.interceptors.push(['$rootScope', '$window', function($rootScope, $window) {
                $rootScope.apiPath = "http://localhost:8080";
                return {
                    request: function(config) {
                        config.headers = config.headers || {};
                        config.withCredentials = true;
                        $httpProvider.defaults.useXDomain = true;
                        delete $httpProvider.defaults.headers.common['X-Requested-With'];
                        return config;
                    }
                }
            }]);
    });


    as.run(['$rootScope', '$http', '$location', '$timeout', '$templateCache', function($rootScope, http, location, timeout, templateCache) {
            /*======================= Registering different login event listners ==============================*/
            // used to show login page 
            $rootScope.isLogin = undefined; // used for maintain session
            $rootScope.showLogin = function() {
                timeout(function() {
                    if (!!$rootScope.isLogin) {
                        location.path("/");
                    } else {
                        $rootScope.isLogin = undefined;
                        location.path('/');
                    }
                });
            };
            $rootScope.goHome = function()
            {
                location.path('/');
            };

            /**
             * On 'event:loginConfirmed', resend all the 401 requests.
             */
            $rootScope.$on('event:loginConfirmed', function() {
                console.log("run : login confirmed event");
                $rootScope.isLogin = true;
//                $rootScope.goHome(); // if you want to redirect to home page
            });

            /**
             * On 'logoutRequest' invoke logout on the server and broadcast 'event:loginRequired'.
             */
            $rootScope.$on('event:logoutRequest', function() {
                console.log("run : logout request event");
                http.post($rootScope.apiPath + '/unauthenticateUser', {}).success(function() {
                    $rootScope.isLogin = false;
                    $rootScope.currentUser = undefined;
                    $rootScope.showLogin();
                });
            });
            /*======================= Login event registration over ===========================================*/


            /**
             * Ping server to figure out if user is already logged in.
             * We have added pinged to a service which will set sessionData.
             */
            $rootScope.pingServer = function() {
                if (location.$$path !== '/recoverpwd') {
                    http.get($rootScope.apiPath + '/common/session').success(function(data) {
                        $rootScope.currentUser = data.currentUser;
                        $rootScope.$broadcast('event:loginConfirmed');
                    });
                }
            };
            $rootScope.getToday = function() {
                return new Date();
            };
            //To authenticate URL's
            $rootScope.$on('$routeChangeStart', function(event, next, current) {

            });
        }]);

}());
